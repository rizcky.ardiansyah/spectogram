package com.groundupai.test.model;

import javax.persistence.*;

@Entity
@Table(name = "reasons")
public class Reasons {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "m_id")
    private int m_id;

    @Column(name = "reason")
    private String reason;

    public Reasons() {
    }

    public Reasons(long id, int m_id, String reason) {
        this.id = id;
        this.m_id = m_id;
        this.reason = reason;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getMid() {
        return m_id;
    }

    public void setMid(int m_id) {
        this.m_id = m_id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

}
