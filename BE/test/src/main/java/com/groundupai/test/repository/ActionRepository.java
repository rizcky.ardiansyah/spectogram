package com.groundupai.test.repository;

import java.util.List;
import com.groundupai.test.model.Actions;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActionRepository extends JpaRepository<Actions, Long> {
    List<Actions> findById(int id);

    List<Actions> findByNameContaining(String name);
}
