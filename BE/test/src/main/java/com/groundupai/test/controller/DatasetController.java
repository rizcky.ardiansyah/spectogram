package com.groundupai.test.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.groundupai.test.FileStorageService;
import com.groundupai.test.message.ResponseFile;
import com.groundupai.test.model.Dataset;
import com.groundupai.test.repository.DatasetRepository;

import javafx.scene.chart.PieChart.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/")
@CrossOrigin
public class DatasetController {
	@Autowired
	DatasetRepository datasetRepository;

	@GetMapping("/datasets")
	public ResponseEntity<List<Dataset>> getAllDatasets() {
		try {
			List<Dataset> datasets__ = new ArrayList<Dataset>();
			// datasetRepository.findAll().forEach(datasets::add);
			List<Dataset> datasets = datasetRepository.findAll();
			for (Dataset dataset : datasets) {
				String fileDownloadUri = ServletUriComponentsBuilder
						.fromCurrentContextPath()
						.path("/files/")
						.path(dataset.getFilename())
						.toUriString();
				dataset.setAction(dataset.getAction());
				dataset.setAnomaly(dataset.getAnomaly());
				dataset.setComment(dataset.getComment());
				dataset.setFilename(dataset.getFilename());
				dataset.setId(dataset.getId());
				dataset.setMachine(dataset.getMachine());
				dataset.setUrl(fileDownloadUri);
				datasets__.add(dataset);
			}

			// datasetRepository.findAll().forEach(datasets::add);

			if (datasets.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(datasets__, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// @GetMapping("/dataset")
	// public ResponseEntity<List<ResponseFile>> getListFiles() {
	// List<ResponseFile> files = storageService.getAllFiles().map(dbFile -> {
	// String fileDownloadUri = ServletUriComponentsBuilder
	// .fromCurrentContextPath()
	// .path("/files/")
	// .path(dbFile.getSensor())
	// .toUriString();

	// return new ResponseFile(
	// dbFile.getSensor(),
	// fileDownloadUri,
	// dbFile.getType(),
	// dbFile.getSoundClip().length);
	// }).collect(Collectors.toList());

	// return ResponseEntity.status(HttpStatus.OK).body(files);
	// }

	@GetMapping("/datasets/{id}")
	public ResponseEntity<Dataset> getTutorialById(@PathVariable("id") long id) {
		Optional<Dataset> datasetData = datasetRepository.findById(id);

		if (datasetData.isPresent()) {
			return new ResponseEntity<>(datasetData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping(value = "/datasets", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<Dataset> createDataset(@RequestPart("dataset") String dataset,
			@RequestPart("file") MultipartFile file) {
		try {
			Dataset tset = new Dataset();
			System.out.println("Dataset :" + dataset);
			System.out.println("File :" + file);
			// JsonObject jsonObject = new JsonParser().parse(dataset).getAsJsonObject();
			JsonObject convertedObject = new Gson().fromJson(dataset, JsonObject.class);
			System.out.println("set data : " + convertedObject.get("timestamp").getAsString());
			String time = convertedObject.get("timestamp").getAsString();
			int machine = convertedObject.get("machine").getAsInt();
			int anomaly = convertedObject.get("anomaly").getAsInt();
			String sensor = convertedObject.get("sensor").getAsString();
			String fileName = StringUtils.cleanPath(file.getOriginalFilename());

			Dataset _dataset = datasetRepository.save(
					new Dataset(time, machine, anomaly, sensor, file.getBytes(), fileName, file.getContentType()));
			System.out.println("save dataset: " + _dataset);
			return new ResponseEntity<>(_dataset, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/datasets/{id}")
	public ResponseEntity<Dataset> updateDataset(@PathVariable("id") long id,
			@RequestBody Dataset dataset) {
		Optional<Dataset> datasetData = datasetRepository.findById(id);

		if (datasetData.isPresent()) {
			Dataset _dataset = datasetData.get();
			_dataset.setReason(dataset.getReason());
			_dataset.setAction(dataset.getAction());
			_dataset.setComment(dataset.getComment());
			return new ResponseEntity<>(datasetRepository.save(_dataset),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/files/{id}")
	public ResponseEntity<byte[]> getFile(@PathVariable String id) {
		Dataset fileDB = datasetRepository.findByFilename(id);

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileDB.getFilename() + "\"")
				.body(fileDB.getSoundClip());
	}

	// @DeleteMapping("/datasets/{id}")
	// public ResponseEntity<HttpStatus> deleteDataset(@PathVariable("id") long id)
	// {
	// try {
	// datasetRepository.deleteById(id);
	// return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	// } catch (Exception e) {
	// return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	// }
	// }

	// @DeleteMapping("/datasets")
	// public ResponseEntity<HttpStatus> deleteAlldatasets() {
	// try {
	// datasetRepository.deleteAll();
	// return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	// } catch (Exception e) {
	// return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	// }

	// }

	// @GetMapping("/datasets/published")
	// public ResponseEntity<List<Dataset>> findByPublished() {
	// try {
	// List<Dataset> datasets = datasetRepository.findByPublished(true);

	// if (datasets.isEmpty()) {
	// return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	// }
	// return new ResponseEntity<>(datasets, HttpStatus.OK);
	// } catch (Exception e) {
	// return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	// }
	// }
}