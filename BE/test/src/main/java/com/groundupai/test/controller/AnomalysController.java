package com.groundupai.test.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.groundupai.test.model.Anomalys;
import com.groundupai.test.repository.AnomalysRepository;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AnomalysController {
    @Autowired
    AnomalysRepository anomalysRepository;

    @GetMapping("/anomalys")
    public ResponseEntity<List<Anomalys>> getAllAnomalys(@RequestParam(required = false) String name) {
        try {
            List<Anomalys> anomalys = new ArrayList<Anomalys>();

            if (name == null)
                anomalysRepository.findAll().forEach(anomalys::add);
            else
                anomalysRepository.findByNameContaining(name).forEach(anomalys::add);

            if (anomalys.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(anomalys, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/anomalys")
    public ResponseEntity<Anomalys> createAnomalys(@RequestBody Anomalys action) {
        try {
            Anomalys _anomalys = anomalysRepository.save(new Anomalys(action.getId(), action.getName()));
            return new ResponseEntity<>(_anomalys, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
