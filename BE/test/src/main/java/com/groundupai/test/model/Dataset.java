package com.groundupai.test.model;

import javax.persistence.*;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "dataset")
public class Dataset {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "time")
    private String time;

    @Column(name = "machine")
    private int machine;

    @Column(name = "anomaly")
    private int anomaly;

    @Column(name = "sensor")
    private String sensor;

    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    @Column(name = "soundclip")
    private byte[] soundclip;

    @Column(name = "suspectedreason")
    private int reason;

    @Column(name = "action")
    private int action;

    @Column(name = "comment")
    private String comment;

    @Column(name = "filename")
    private String filename;

    @Column(name = "type")
    private String type;
    private String url;

    public Dataset() {
    }

    public Dataset(String time, int machine, int anomaly, String sensor) {
        this.time = time;
        this.machine = machine;
        this.anomaly = anomaly;
        this.sensor = sensor;
    }

    public Dataset(String time, int machine, int anomaly, String sensor, byte[] soundclip, String filename,
            String type) {
        this.time = time;
        this.machine = machine;
        this.anomaly = anomaly;
        this.sensor = sensor;
        this.filename = filename;
        this.type = type;
        this.soundclip = soundclip;
    }

    public Dataset(long id, String time, int machine, int anomaly, String sensor, byte[] soundclip, String filename,
            String type, int reason, int action, String comment, String url) {
        this.id = id;
        this.time = time;
        this.machine = machine;
        this.anomaly = anomaly;
        this.sensor = sensor;
        this.filename = filename;
        this.type = type;
        this.reason = reason;
        this.action = action;
        this.comment = comment;
        this.soundclip = soundclip;
        this.url = url;
    }

    public Dataset(int reason, int action, String comment) {
        this.reason = reason;
        this.action = action;
        this.comment = comment;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getReason() {
        return reason;
    }

    public void setReason(int reason) {
        this.reason = reason;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSensor() {
        return sensor;
    }

    public void setSensor(String sensor) {
        this.sensor = sensor;
    }

    public String gettime() {
        return time;
    }

    public void settime(String time) {
        this.time = time;
    }

    public int getMachine() {
        return machine;
    }

    public void setMachine(int machine) {
        this.machine = machine;
    }

    public int getAnomaly() {
        return anomaly;
    }

    public void setAnomaly(int anomaly) {
        this.anomaly = anomaly;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public byte[] getSoundClip() {
        return soundclip;
    }

    public void setSoundClip(byte[] soundClip) {
        this.soundclip = soundClip;
    }

    // public long getId() {
    // return id;
    // }

    // public String getTitle() {
    // return title;
    // }

    // public void setTitle(String title) {
    // this.title = title;
    // }

    // public String getDescription() {
    // return description;
    // }

    // public void setDescription(String description) {
    // this.description = description;
    // }

    // public boolean isPublished() {
    // return published;
    // }

    // public void setPublished(boolean isPublished) {
    // this.published = isPublished;
    // }

    // @Override
    // public String toString() {
    // return "Tutorial [id=" + id + ", title=" + title + ", desc=" + description +
    // ", published=" + published + "]";
    // }
}
