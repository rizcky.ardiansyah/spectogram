package com.groundupai.test.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.groundupai.test.model.Dataset;

public interface DatasetRepository extends JpaRepository<Dataset, Long> {

    List<Dataset> findByid(int id);

    Dataset findByFilename(String filename);

    Dataset findByid(Long id);

    List<Dataset> findByMachineContaining(int machine);
}
