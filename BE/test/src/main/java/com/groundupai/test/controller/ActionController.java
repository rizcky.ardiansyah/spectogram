package com.groundupai.test.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.groundupai.test.model.Actions;
import com.groundupai.test.repository.ActionRepository;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ActionController {
    @Autowired
    ActionRepository actionRepository;

    @GetMapping("/actions")
    public ResponseEntity<List<Actions>> getAllActionss(@RequestParam(required = false) String name) {
        try {
            List<Actions> actions = new ArrayList<Actions>();

            if (name == null)
                actionRepository.findAll().forEach(actions::add);
            else
                actionRepository.findByNameContaining(name).forEach(actions::add);

            if (actions.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(actions, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/actions")
    public ResponseEntity<Actions> createAction(@RequestBody Actions action) {
        try {
            Actions _actions = actionRepository.save(new Actions(action.getId(), action.getName()));
            return new ResponseEntity<>(_actions, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
