package com.groundupai.test.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.groundupai.test.model.Reasons;

public interface ReasonRepository extends JpaRepository<Reasons, Long> {
    List<Reasons> findById(int id);

    List<Reasons> findByReasonContaining(String reason);
}
