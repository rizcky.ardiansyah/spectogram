package com.groundupai.test.message;

public class ResponseFile {
    private String name;
    private String url;
    private String type;
    private long size;
    private String time;
    private int machine;
    private int anomaly;
    private String sensor;

    public ResponseFile(String name, String url, String type, long size, String time, int machine, int anomaly,
            String sensor) {
        this.name = name;
        this.url = url;
        this.type = type;
        this.size = size;
        this.time = time;
        this.machine = machine;
        this.anomaly = anomaly;
        this.sensor = sensor;
    }

    public String getSensor() {
        return sensor;
    }

    public void setSensor(String sensor) {
        this.sensor = sensor;
    }

    public String gettime() {
        return time;
    }

    public void settime(String time) {
        this.time = time;
    }

    public int getMachine() {
        return machine;
    }

    public void setMachine(int machine) {
        this.machine = machine;
    }

    public int getAnomaly() {
        return anomaly;
    }

    public void setAnomaly(int anomaly) {
        this.anomaly = anomaly;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}
