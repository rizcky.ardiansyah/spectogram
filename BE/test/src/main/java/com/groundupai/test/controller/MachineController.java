package com.groundupai.test.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.groundupai.test.model.Machine;
import com.groundupai.test.repository.MachineRepository;

@RestController
@RequestMapping("/")
@CrossOrigin
public class MachineController {
    @Autowired
    MachineRepository machineRepository;

    @GetMapping("/machines")
    public ResponseEntity<List<Machine>> getAllMachine(@RequestParam(required = false) String name) {
        try {
            List<Machine> machines = new ArrayList<Machine>();

            if (name == null)
                machineRepository.findAll().forEach(machines::add);
            else
                machineRepository.findByNameContaining(name).forEach(machines::add);

            if (machines.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(machines, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/machines")
    public ResponseEntity<Machine> createMachine(@RequestBody Machine machine) {
        try {
            Machine _machines = machineRepository.save(new Machine(machine.getId(), machine.getName()));
            return new ResponseEntity<>(_machines, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
