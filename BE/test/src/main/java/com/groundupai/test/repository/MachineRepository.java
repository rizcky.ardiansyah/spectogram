package com.groundupai.test.repository;

import java.util.List;
import com.groundupai.test.model.Machine;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MachineRepository extends JpaRepository<Machine, Long> {
    List<Machine> findById(int id);

    List<Machine> findByNameContaining(String name);
}
