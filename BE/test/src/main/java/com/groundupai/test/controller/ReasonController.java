package com.groundupai.test.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.groundupai.test.model.Reasons;
import com.groundupai.test.repository.ReasonRepository;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ReasonController {
    @Autowired
    ReasonRepository reasonRepository;

    @GetMapping("/reasons")
    public ResponseEntity<List<Reasons>> getAllReason(@RequestParam(required = false) String reason) {
        try {
            List<Reasons> reasons = new ArrayList<Reasons>();

            if (reason == null)
                reasonRepository.findAll().forEach(reasons::add);
            else
                reasonRepository.findByReasonContaining(reason).forEach(reasons::add);

            if (reasons.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(reasons, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/reasons")
    public ResponseEntity<Reasons> createMachine(@RequestBody Reasons reasons) {
        try {
            Reasons _reason = reasonRepository
                    .save(new Reasons(reasons.getId(), reasons.getMid(), reasons.getReason()));
            return new ResponseEntity<>(_reason, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
