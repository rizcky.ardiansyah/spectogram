package com.groundupai.test.repository;

import java.util.List;
import com.groundupai.test.model.Anomalys;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AnomalysRepository extends JpaRepository<Anomalys, Long> {
    List<Anomalys> findById(int id);

    List<Anomalys> findByNameContaining(String name);
}
